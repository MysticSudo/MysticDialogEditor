existingNodes = [];
class EditorNode
  constructor: (@name="node name",@type = 0,@contentTemplate) ->
    @id = Math.Random*9999+"_"
    existingNodes.push(this);

  CompileWithContent: (@content) ->
    Mustache.parse(@contentTemplate);
    @innerHtml = Mustache.render(@contentTemplate, @content);

  GenerateID: (length) ->
    ret = ""
    possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for i in [0..length]
      ret += possible.charAt(Math.floor(Math.random() * possible.length));
    return ret;
