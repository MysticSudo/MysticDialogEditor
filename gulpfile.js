var gulp = require('gulp'),
minify = require('gulp-minify'),
coffee = require('gulp-coffee'),
concat = require('gulp-concat'),
less = require('gulp-less'),
path = require('path'),
watch = require('gulp-watch');
gulp.task('default', function()
{
  watch('js/**/*.js', function () {
    compile();
  });
  watch('js/**/*.coffee', function () {
    compile();
  });
  watch('less/**/*.less', function () {
    gulp.src('less/**/*.less')
    .pipe(less({
      paths: [ path.join(__dirname, 'less', 'includes') ]
    }))
    .pipe(concat('master.css'))
    .pipe(gulp.dest('css'));
  });
});
function compile()
{

  gulp.src('js/**/*.coffee')
  .pipe(coffee({bare: true}))
  .pipe(concat('coffee-compiled/coffee.js'))
  .pipe(gulp.dest('js'));
  gulp.src('js/**/*.js')
  .pipe(concat('index.js'))
  .pipe(minify())
  .pipe(gulp.dest('dist'));
}
