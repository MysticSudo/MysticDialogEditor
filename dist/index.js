var fs = require("fs");

$(function()
{
  Resize();
  $(window).resize(function()
  {
    Resize();
  })
})
function Resize()
{
  console.log("Resizing");
  $(".window-height").css("height",$(window).innerHeight());
}


function createNewNode()
{

}

var EditorNode, existingNodes;

existingNodes = [];

EditorNode = (function() {
  function EditorNode(name, type, contentTemplate) {
    this.name = name != null ? name : "node name";
    this.type = type != null ? type : 0;
    this.contentTemplate = contentTemplate;
    this.id = Math.Random * 9999 + "_";
    existingNodes.push(this);
  }

  EditorNode.prototype.CompileWithContent = function(content) {
    this.content = content;
    Mustache.parse(this.contentTemplate);
    return this.innerHtml = Mustache.render(this.contentTemplate, this.content);
  };

  EditorNode.prototype.GenerateID = function(length) {
    var i, j, possible, ref, ret;
    ret = "";
    possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (i = j = 0, ref = length; 0 <= ref ? j <= ref : j >= ref; i = 0 <= ref ? ++j : --j) {
      ret += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return ret;
  };

  return EditorNode;

})();

var OptionNode,
  extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

OptionNode = (function(superClass) {
  extend(OptionNode, superClass);

  function OptionNode() {}

  return OptionNode;

})(EditorNode);
